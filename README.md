[English](#conversion-of-csv-to-genopro-xml) | [Slovensky](#konverzia-csv-do-genopro-xml) | csv2gnoxml v0.0.1

# Konverzia .csv do GenoPro .xml
Tento UNIX/Linux skript (moŽno funguje aj na macOS/BSD, no tam to nie je testované) je určený na konverziu súborov .csv exportovaných zo stránky [rodokmen.sk](rodokmen.sk) do formátu .xml, ktorý sa dá importovať do programu [GenoPro](genopro.com).

Daný skript testujem *výlučne* na Linuxe, aktuálne konkrétne na distríbúcii Kubuntu 17.10. V prípade potreby a dostatku ĉasu som ochotný ho otestovať, príp. spojazdniť na iných distribúciách Linuxu, na Windowse (ktorejkoľvek verzii, či už cez [VirtualBox](virtualbox.org). Win 10 WSL alebo [Cygwin](cygwin.com) a jeho alternatívy). Ku macOS/BSD nemám prístup, no cez Net poradím a pomôžem, ako budem môcť.

## Ako skonvertujem .csv do .xml
[1] V prvom rade, potrebujete mať spustený UNIX alebo Linux operačný systém. Buď si ho nainštalujete na reálne (hdd, ssd alebo usb) alebo na virtuálne úložisko (napr. [VirtualBox](virtualbox.org), alebo spustíte Live distribúciu (napr. [Ubuntu](ubuntu.com)) z cd/dvd/usb, príp. vo Windowse ho možné sputiť aj cez [Cygwin](cygwin.com) a jeho alternatívy (znova: netestované). Postupy na spustenie UNIX-u/Linux-u/Cygwinu nájdete na Nete.

[2] Je nutné nainštalovať dependencie (závislostí).

[3] Samotná konverzia.

## Známe chyby

## Neviete si poradiť a potrebujete pomoc so skriptom?
[1] Najskôr prehľadajte už podané chyby (issues), či už daný problém niekto iný nenahlasoval, príp či sa už riešenie na daný problém nenašlo.

[2] Ak ste nenašli taký istý problém, vytvorte nové podanie chyby (issue) s nasledujúcimi informáciami:
- operačný systém, verzia OS,
- verzia skriptu,
- čo najdetainejší popis chyby/problému.

[3] Prípadne obdobným spôsobom môžete požiadať aj o novú funkciu/vlastnosť (feature) skriptu.

## Zoznam XML tagov a hlavičky súboru .csv
[gSheets](https://docs.google.com/spreadsheets/d/1j4mpOVb8FFplzuOcnWkz5l6ZZDJ3Y1G2AJG3sbBpn_I/edit?usp=sharing)


-------------------------------------

# Conversion of .csv to GenoPro .xml
This UNIX/Linux script (might work on macOS/BSD, but it is not tested) is used to convert .csv files exported from [rodokmensk](rodomen.sk) to .xml file format, which can be imported to [GenoPro](genopro.com) software.

I test the script *only* on Linux, at the moment on Kubuntu 17.10 distribution. I case of need of help, while I have enough time, I am willing to test it, or even help to make it run on other Linux distributions, on any version of Windows (either through [VirtualBox](virtualbox.org) or [Cygwin](cygwin.com) and its alternatives). I have no access to macOS/BSD, but via Net I will help as much as I can.

## How to Convert .csv to .xml
[1] Firstly, you need UNIX or Linux operating system installed. You can install in on real storage (hdd, sdd or usb) or on virtual storage (such as [VirtualBox](virtualbox.org) or boot a Live distribution (e.g. [Ubuntu](ubuntu.com) from cd/dvd/usb/, or on Windows you can run it also through [Cygwin](cygwin.com) and its alternatives (again: their not tested). You can find the tutorials to boot UNIX/Linux/Cygwin on the Net.

[2] It is necessary to install dependecies.

[3] The conversion itself.

## Known Issues
## Can’t Deal with a Problem and Need Help with the Script?
[1] Firstly, search the already reported issues if someone else has already found such a problem, or even if there is a solution for it.

[2] If you haven’t found same problem, file a new issue with these information:
- operating system and its version,
- script version,
- as detailed issue description as possible,

[3] This way you can file a feature request, too.

## List of All XML Tags and the Header of .csv
[gSheets](https://docs.google.com/spreadsheets/d/1j4mpOVb8FFplzuOcnWkz5l6ZZDJ3Y1G2AJG3sbBpn_I/edit?usp=sharing)