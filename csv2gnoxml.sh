#!/bin/bash

# csv2gnoxml

# author: Tukusej's Sirs
# version: 0.0.2

# dependencies: printf seq date cat grep sed echo

# Help to change {full,display}NameFormat:
# - %T  : title (prefix)
# - %F  : first name
# - %N  : nickname
# - %M  : middle name
# - %L  : last name (first last name in life, i.e. maiden name for women)
# - %L2 : second last name (i.e. for women, the surname received from husband)
# - %S  : suffix

# TODO:
# - function to generate individuals ids (ind#####)
# - function to generate places ids (place#####)

# HARDEST THINGS TODO:
# - calculate position of people, families


# Just to be sure there are no aliases, I use the binaries with absolute path
#printf=$(which printf)  # For now, /usr/bin/printf does not work the same way as printf; I have no idea why - probably a different versions?
printf="printf"
date=$(which date)
seq=$(which seq)

# CSV related vars
csvFileName="input.csv"
csvRecordsNumber=$(cat $csvFileName | grep -v "^#" | sed "1 d" | wc -l)  # src: http://bconnelly.net/working-with-csvs-on-the-command-line/#determining-the-number-of-rows

# XML related vars
xmlFileName="family_tree"
tabSize=2  # Size of tab; the exported GenoPro XML uses two spaces as tab, therefore it is default
tab=$($printf "%0.s " $($seq 1 $tabSize))  # src: https://stackoverflow.com/a/17030976/3408342
newline="\n"  # Newline type; as I work on Linux, default is \n
version="3.0.1.0"  # GenoPro version; 3.0.1.0 is the version I used
date=$($date +"%d %b %Y")  # Default is current date
fullNameFormat="%T %F (%N) %M %L (%L2) %S"  # Default is "%T %F (%N) %M %L (%L2) %S" as it was exported from GenoPro to XML
displayNameFormat="%F %M %L (%L2)"  # Default is "%F %M %L (%L2)" as it was exported from GenoPro to XML
displayNameLines=3  # Default is 3; this settings is about number of lines in displaying the display name in the genogram
font="Arial"  # Default is Arial as it was exported from GenoPro to XML; font but be installed on local computer
colourGenderSymbol="#000000"
colourGenderText="#000000"
colourGenderFill="#FFFFFF"
colourGenderFillTopLeft="#FF0000"
colourGenderFillTopRight="#FF0000"
colourGenderFillBottomLeft="#FF0000"
colourGenderFillBottomRight="#FF0000"
colourBorderOutline="#6E6EFF"
colourBorderFill="#FFFF80"
colourLabelTop="#000000"
colourLabelBottom="#000000"
colourLabelFillTop="#FFFFFF"
colourLabelFillBottom="#FFFFFF"
genoMapName="GenoMap1"  # Default is "GenoMap1"; is the the name of genomap and also the name of the 'sheet', which contains the genomap
genoMapZoom="105"
genoMapPosition="-417,-109"
genoMapBoundaryRect="-564,94,450,-175"
lastUsedIndID=0
tempIndID=0

# XML Header
echo -e "<?xml version='1.0' encoding='UTF-8'?>$newline<GenoPro>$newline$tab<Software>GenoPro$newline$tab$tab<Name>GenoPro® - Picture Your Family Tree!(TM)</Name>$newline$tab$tab<Version>$version</Version>$newline$tab$tab<Address>http://www.genopro.com</Address>$newline$tab</Software>$newline$tab<Date>$date</Date>" > $xmlFileName.xml

# XML Global
echo -e "<Global>$newline$tab<Name>$newline$tab$tab<Full Format=\"$fullNameFormat\"/>$newline$tab$tab<Display Format=\"$displayNameFormat\" Lines=\"$displayNameLines\"/>$newline$tab</Name>$newline$tab<Font>$font</Font>$newline$tab<Display>$newline$tab$tab<Tag>YoB_YoD</Tag>$newline$tab$tab<Colors>$newline$tab$tab$tab<Gender Symbol=\"$colourGenderSymbol\" Text=\"$colourGenderText\" Fill=\"$colourGenderFill\">$newline$tab$tab$tab$tab<Fill>$newline$tab$tab$tab$tab$tab<Top Left=\"$colourGenderFillTopLeft\" Right=\"$colourGenderFillTopRight\"/>$newline$tab$tab$tab$tab$tab<Bottom Left=\"$colourGenderFillBottomLeft\" Right=\"$colourGenderFillBottomRight\"/>$newline$tab$tab$tab$tab</Fill>$newline$tab$tab$tab</Gender>$newline$tab$tab$tab<Border Outline=\"$colourBorderOutline\">$newline$tab$tab$tab$tab<Fill>$colourBorderFill</Fill>$newline$tab$tab$tab</Border>$newline$tab$tab$tab<Label Top=\"$colourLabelTop\" Bottom=\"$colourLabelBottom\">$newline$tab$tab$tab$tab<Fill Top=\"$colourLabelFillTop\" Bottom=\"$colourLabelFillBottom\"/>$newline$tab$tab$tab</Label>$newline$tab$tab</Colors>$newline$tab</Display>$newline$tab<ActiveGenoMap>$genoMapName</ActiveGenoMap>$newline</Global>$newline<GenoMaps>$newline$tab<GenoMap Name=\"$genoMapName\" Zoom=\"105\" Position=\"-417,-109\" BoundaryRect=\"-564,94,450,-175\"/>$newline</GenoMaps>" >> $xmlFileName.xml

# XML Individuals
lastUsedIndID=$((lastUsedIndID+1))
tempID=$(printf "%05d" $lastUsedIndID)

echo -e "$newline<Individual ID=\"ind$tempID\">$newline$tab<Name>Janka Stredne Zelerova (Mrkvickova)
    <Display>Janka Stredne Zelerova (Mrkvickova)</Display>
    <First>Janka</First>
    <Middle>Stredne</Middle>
    <Last>Zelerova</Last>
    <Last2>Mrkvickova</Last2>
  </Name>
  <Position BoundaryRect="-394,60,-307,-25">-350,30</Position>
  <Gender>F</Gender>
  <Birth>
    <Date>1 Jan 1991</Date>
    <Place>place00001</Place>
  </Birth>
  <IsDead>Y</IsDead>
  <Death>
    <Date>1 Jan 2018</Date>
    <Place>place00001</Place>
    <Comment>Poznamka k umrtiu</Comment>
  </Death>
  <Comment>Poznamky (vseobecne ku osobe)</Comment>
</Individual>


#<Individual ID="ind00002">
#  <Name>Janka Stredne Zelerova (Mrkvickova)
#    <Display>Janka Stredne Zelerova (Mrkvickova)</Display>
#    <First>Janka</First>
#    <Middle>Stredne</Middle>
#    <Last>Zelerova</Last>
#    <Last2>Mrkvickova</Last2>
#  </Name>
#  <Position BoundaryRect="-394,60,-307,-25">-350,30</Position>
#  <Gender>F</Gender>
#  <Birth>
#    <Date>1 Jan 1991</Date>
#    <Place>place00001</Place>
#  </Birth>
#  <IsDead>Y</IsDead>
#  <Death>
#    <Date>1 Jan 2018</Date>
#    <Place>place00001</Place>
#    <Comment>Poznamka k umrtiu</Comment>
#  </Death>
#  <Comment>Poznamky (vseobecne ku osobe)</Comment>
#</Individual>

# XML Families

# XML Pedigree links

# XML Places

# XML Labels

# XML end tag of <GenoPro>
#echo </GenoPro> >> $xmlFileName.xml
